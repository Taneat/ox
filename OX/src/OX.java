import java.util.Scanner;

public class OX {
	private char[][] board; 
    private char currentPlayer;
    public void initializeBoard() {
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                board[i][j] = '-';
            }
        }
    }
    public OX() {
        board = new char[4][4];
        currentPlayer = 'x';
        initializeBoard();
    }
    public void showWelcome() {
    	System.out.println("Welcome to OX Game");
    }
    public void printCurrentBoard() {
    	System.out.println("Current board");
    }
    public void printBoard() {
    	System.out.println("-------------");
    	for (int i = 1; i < 4; i++) {
    		System.out.print("| ");
    		for (int j = 1; j < 4; j++) {
    			System.out.print(board[i][j] + " | ");
    	    }
    		System.out.println();
    		System.out.println("-------------");
    	 }
    }
    public boolean isBoardFull() {
        boolean isFull = true;
        for (int i = 1; i < 4; i++) {
            for (int j = 1; j < 4; j++) {
                if (board[i][j] == '-') {
                    isFull = false;
                }
            }
        }
        return isFull;
    }
    private boolean checkRowCol(char c1, char c2, char c3) {
    	return ((c1 != '-') && (c1 == c2) && (c2 == c3));
    }
    private boolean checkRowsForWin() {
    	for (int i = 1; i < 4; i++) {
    		if (checkRowCol(board[i][1], board[i][2], board[i][3]) == true) {
    			return true;
    		}
    	}
    	return false;
    }
    private boolean checkColumnsForWin() {
    	for (int i = 1; i < 4; i++) {
    		if (checkRowCol(board[1][i], board[2][i], board[3][i]) == true) {
    			 return true;
    		}
    	}
    	return false;
    }
    private boolean checkDiagonalsForWin() {
    	return ((checkRowCol(board[1][1], board[2][2], board[3][3]) == true) || (checkRowCol(board[1][3], 
    			board[2][2], board[3][1]) == true));
    }
    public boolean checkForWin() {
    	return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
    }
    public void changePlayer() {
    	if (currentPlayer == 'x') {
    		currentPlayer = 'o';
    	}else {
    		currentPlayer = 'x';
    	}
    }
    public boolean pleaseInput (int row, int col) {
    	if ((row >= 1) && (row < 4)) {
    		 if ((col >= 1) && (col < 4)) {
    			 if (board[row][col] == '-') {
    				 board[row][col] = currentPlayer;
    				 return true;
    			 }
    		 }
    	}
    	return false;
    }
    public static void main(String[] args) {
    	Scanner kb = new Scanner(System.in);
    	OX game = new OX();
    	game.initializeBoard();
    	game.showWelcome();
    	do {
    		game.printCurrentBoard();
    		game.printBoard();
    		int row,col;
    		do {
    			System.out.print("Player " + game.currentPlayer + " Please input row,col: ");
    			row = kb.nextInt();
    			col = kb.nextInt();
    		}
    		while (!game.pleaseInput(row, col));
    		game.changePlayer();
    	}
    	while(!game.checkForWin() && !game.isBoardFull());
    	if(game.isBoardFull() && !game.checkForWin()) {
    		game.printCurrentBoard();
    		game.printBoard();
    		System.out.println();
    		System.out.println("Draw!");
    	}else {
    		game.printCurrentBoard();
    		game.printBoard();
    		game.changePlayer();
    		System.out.println(Character.toUpperCase(game.currentPlayer) + " Win!");
    	}
    }
}
